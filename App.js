import React from 'react';
import { View, Text,Button,SafeAreaView,Easing,Animated,Image,FlatList,StyleSheet,RefreshControl,ScrollView,TouchableOpacity,StatusBar,TextInput } from 'react-native';
import { createStackNavigator,createBottomTabNavigator,createDrawerNavigator,createSwitchNavigator } from 'react-navigation';
import { Ionicons,FontAwesome } from '@expo/vector-icons';
import { LinearGradient,Camera, Permissions } from 'expo';

class LogoTitle extends React.Component {
    render() {
        return (
            <Image
                source={require('./file.jpg')}
                style={{ width: 130, height: 40 }}
            />
        );
    }
}

const HEADER_MAX_HEIGHT = 150;
const HEADER_MIN_HEIGHT = 100;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 22,
        position: 'relative'
    },
    item: {
        width: 300,
        height: 200
    },
    fill: {
        flex: 1,
        backgroundColor:'#fff',
    },
    row: {
        height: 40,
        margin: 16,
        backgroundColor: '#D3D3D3',
        alignItems: 'center',
        justifyContent: 'center',
    },
    header: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        borderBottomColor:'#f4f4f4',
        backgroundColor: '#fff',
        borderBottomWidth:1,
    },
    bar: {
        paddingTop: 45,
        alignItems: 'center',
        justifyContent: 'center',
        width:100 + '%',
        paddingLeft:15,
        paddingRight:15,
        height: 100 + '%',
        position:'relative'
    },
    title: {
        backgroundColor: 'transparent',
        color: 'white',
        fontSize: 18,
    },
    scrollViewContent: {
        marginTop: HEADER_MAX_HEIGHT,
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        height: HEADER_MAX_HEIGHT,
        resizeMode: 'cover',
    },
})

class Header extends React.Component {

    _renderScrollViewContent() {
        const data = Array.from({length: 30});
        return (
            <View style={styles.scrollViewContent}>
                {data.map((_, i) =>
                    <View key={i} style={styles.row}>
                        <Text>{i}</Text>
                    </View>
                )}
            </View>
        );
    }

    constructor(props) {
        super(props);

        this.state = {
            scrollY: new Animated.Value(0),
        };
    }


    render() {
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
            extrapolate: 'clamp',
        });

        const imageOpacity = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 1, 0],
            extrapolate: 'clamp',
        });
        const imageTranslate = this.state.scrollY.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE],
            outputRange: [0, -50],
            extrapolate: 'clamp',
        });


        return (
            <SafeAreaView style={styles.fill}>
                <ScrollView
                    scrollEventThrottle={16}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}]
                    )}
                    style={styles.fill}
                >
                    {this._renderScrollViewContent()}
                </ScrollView>
                <Animated.View style={[styles.header, {height: headerHeight}]}>

                    <View style={styles.bar}>
                        <Animated.View style={{width: 100+'%',opacity: imageOpacity, marginTop:imageTranslate}}>
                            <TextInput
                                style={{height: 40,paddingLeft:15,backgroundColor:'#f4f4f4',borderRadius:10}}
                                placeholder='Search'
                                onChangeText={(text) => this.setState({text})}
                                value={this.state.text}
                            />
                        </Animated.View>
                        <ScrollView style={{height: 100 + '%',width: 100 + '%'}} horizontal={true} showsHorizontalScrollIndicator={false}>
                            <View style={{width: 200,borderRadius: 10,position:'relative',overflow:'hidden'}}>
                                <Image style={styles.backgroundImage} source={{uri: 'https://cdn.pixabay.com/photo/2018/05/24/11/15/landscape-3426391_960_720.jpg'}}/>
                            </View>

                        </ScrollView>
                    </View>
                </Animated.View>
            </SafeAreaView>
        );
    }
}


class Card extends React.Component {

    render(){
        return(
            <View style={{ width: 100 + '%'}}>
                <View style={{ flex: 1,justifyContent:'space-between',flexDirection:'row',padding: 10,borderBottomColor: '#f4f4f4',borderBottomWidth :1}}>
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <Image style={{width:40, height: 40, borderRadius: 20}} source={{uri: 'https://howoldis.ru/images/595.png'}}/>
                        <View style={{paddingLeft: 10}}>
                            <Text style={{fontWeight: 'bold'}}>Lord ash</Text>
                            <Text>Mister</Text>
                        </View>
                    </View>

                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <FontAwesome name="ellipsis-h" size={15}></FontAwesome>
                    </View>
                </View>
                <Image style={{height:400,width:100 + '%'}} source={{uri: this.props.img}}/>

                <View style={{padding: 10}}>
                    <View style={{flex: 1,flexDirection: 'row',justifyContent: 'space-between'}}>
                        <View style={{flexDirection: 'row',alignItems:'center'}}>
                            {this.props.liked ? <FontAwesome name="heart" size={32} color={'red'} style={{marginRight:15}} /> : <FontAwesome style={{marginRight:15}} name="heart-o" size={32} />}
                            <FontAwesome name="comment-o" size={32} style={{marginRight:15}}/>
                            <FontAwesome name="paper-plane-o" size={32} />
                        </View>
                        <View>
                            <FontAwesome name="bookmark-o" size={32} />
                        </View>
                    </View>

                    <View style={{marginTop: 10}}>
                        <Text style={{fontWeight: 'bold'}}>Нравится: 12</Text>
                    </View>
                    <View style={{marginTop: 5}}>
                        <Text style={{fontWeight: 'bold'}}>Lord ash <Text style={{fontWeight: 'normal'}}>Ставим лайк Ребята из @sper_shop знают толк в новинках</Text></Text>
                    </View>
                </View>

            </View>
        )
    }

}

class HomeScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: (
                <TouchableOpacity onPress={() => navigation.navigate('Camera',{direction: 'left'})}>
                  <FontAwesome name="camera" size={25}
                      style={{paddingLeft:15}}

                  />
                </TouchableOpacity>
            ),
            headerRight: (

                <TouchableOpacity onPress={() => navigation.navigate('Direct',{direction: 'right'})}>
                <View style={{position:'relative'}}>

                <FontAwesome name="envelope-open-o" size={25}

                    style={{paddingRight:15}}
                />

                    <View style={{position: 'absolute',right:5,top:-5,width: 20,height:20,borderRadius:10,zIndex:2,alignItems:'center'}}>
                        <LinearGradient
                            colors={['#00b7ea', '#c31c78']}
                            style={{ alignItems: 'center',borderRadius:10,width: 20,height:20 }}>
                          <View style={{ flex: 1,alignItems:'center',flexDirection:'row'}}>
                              <Text style={{color: '#fff',fontWeight: 'bold',fontSize: 10}}>15</Text>
                          </View>
                        </LinearGradient>
                    </View>

                </View>
                </TouchableOpacity>
            ),
            headerTitle: <LogoTitle/>
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
        };
    }

    _onRefresh = () => {
        this.setState({refreshing: true});

        setTimeout(() => {
            this.setState({refreshing: false});
        },5000);
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

                    <ScrollView style={{width: 100 + '%'}} refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }>

                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{borderBottomColor: '#f4f4f4',borderBottomWidth :1,padding: 10}}>

                            <View style={{flexDirection:'row'}}>
                                <View style={{alignItems:'center',marginRight:20,justifyContent:'space-between'}}>
                                    <View style={{position: 'relative',height: 60, width: 60,alignItems:'center',justifyContent:'center'}}>
                                        <View style={{ backgroundColor:'#42a7fe',alignItems:'center',justifyContent:'center',position:'absolute',width: 20,height:20,borderRadius:10,zIndex:2,right:0,bottom:0,borderWidth:2,borderColor:'#fff'}}>
                                            <Text style={{color:'#fff'}}>+</Text>
                                        </View>
                                        <Image style={{width:56, height: 56, borderRadius: 28}} source={{uri: 'https://howoldis.ru/images/595.png'}}/>
                                    </View>
                                    <Text style={{fontSize:12,paddingTop: 5}}>Ваша исто...</Text>
                                </View>
                                <View style={{alignItems:'center',marginRight:20}}>
                                    <LinearGradient
                                        colors={['#fdf497', '#fdf497', '#fd5949', '#d6249f', '#285AEB', '#285AEB']}
                                        style={{ height: 60, width: 60, alignItems: 'center', justifyContent: 'center',borderRadius: 30}}
                                    >
                                        <Image style={{width:56, height: 56, borderRadius: 28,borderColor:'#fff',borderWidth:3}} source={{uri: 'https://im0-tub-ru.yandex.net/i?id=2624b334f7fd32f822486ada45de552f&n=33&w=449&h=300'}}/>
                                    </LinearGradient>
                                    <Text style={{fontSize:12,paddingTop: 5}}>xmee</Text>
                                </View>
                                <View style={{alignItems:'center',marginRight:20}}>
                                    <View style={{flex:1,alignItems:'center',width:60,height:60,borderRadius:30,borderWidth: 1,borderColor:'#e1e1e1',justifyContent:'center'}}>
                                        <Image style={{width:56, height: 56, borderRadius: 28,borderColor:'#fff',borderWidth:3}} source={{uri: 'http://s1.fotokto.ru/photo/large/36/366936.jpg'}}/>
                                    </View>
                                    <Text style={{fontSize:12,paddingTop: 5}}>save_yours...</Text>
                                </View>
                                <View style={{alignItems:'center',marginRight:20}}>
                                    <View style={{flex:1,alignItems:'center',width:60,height:60,borderRadius:30,borderWidth: 1,borderColor:'#e1e1e1',justifyContent:'center'}}>
                                        <Image style={{width:56, height: 56, borderRadius: 28,borderColor:'#fff',borderWidth:3}} source={{uri: 'https://1.bp.blogspot.com/-JXlEZryghiY/WkwAo1IBwUI/AAAAAAAAAtE/parpdj6P9YwJWDmn6JyXrTkHoN38aflMwCLcBGAs/s1600/fins1217.jpg'}}/>
                                    </View>
                                    <Text style={{fontSize:12,paddingTop: 5}}>save_yours...</Text>
                                </View>
                                <View style={{alignItems:'center',marginRight:20}}>
                                    <View style={{flex:1,alignItems:'center',width:60,height:60,borderRadius:30,borderWidth: 1,borderColor:'#e1e1e1',justifyContent:'center'}}>
                                        <Image style={{width:56, height: 56, borderRadius: 28,borderColor:'#fff',borderWidth:3}} source={{uri: 'https://i.artfile.me/wallpaper/09-02-2018/346x230/devushka-devushki--unsort--bryunetki-tem-1302933.jpg'}}/>
                                    </View>
                                    <Text style={{fontSize:12,paddingTop: 5}}>xmee</Text>
                                </View>
                            </View>

                        </ScrollView>
                    <FlatList
                        style={{width: 100 + '%'}}

                        data={[
                            {liked: true,key: 'https://img2.badfon.ru/original/1360x768/d/36/amelie-emili-devushka-748.jpg'},
                            {liked: false,key: 'http://on-desktop.com/wps/World___Italy_Alps_in_summer_at_the_ski_resort_of_Val_Gardena__Italy_062903_.jpg'},

                        ]}
                        renderItem={({item}) => <Card img={item.key} liked={item.liked}/>}
                    />
                    </ScrollView>
                </View>
            </SafeAreaView>
        );
    }
}



class CameraScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'Camera',
            gesturesEnabled: true,
            gestureDirection: 'inverted',
            headerTransparent:true,
            headerRight: (
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <FontAwesome name="arrow-right" size={25} color={'#FFF'} style={{paddingRight:15}}

                    />
                </TouchableOpacity>
            ),
            headerLeft: (
                <TouchableOpacity>
                    <FontAwesome name="cog" size={30} color={'#FFF'} style={{paddingLeft:15}}

                    />
                </TouchableOpacity>
            ),
            /* No more header config here! */
        };
    }

    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
    }

    render() {
        return (
            <View style={{flex: 1,backgroundColor: 'transparent'}}>
                <StatusBar
                    barStyle="dark-content"
                    backgroundColor="transparent"
                />
                <Camera style={{flex:1}} type={Camera.Constants.Type.back}/>

            </View>
        );
    }
}


class Search extends React.Component {
    static navigationOptions = {
        title: 'Search',
        /* No more header config here! */
    };


    render() {
        return (
            <Header></Header>
        );
    }
}

class Direct extends React.Component {
    static navigationOptions = {
        title: 'Direct',
        /* No more header config here! */
    };

    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text>Direct Screen</Text>
                </View>
            </SafeAreaView>
        );
    }
}

let DinamicTransition = (currentState) => {
    const params = currentState.scenes[currentState.scenes.length - 1].route.params;

    if (currentState.scenes[currentState.scenes.length - 1].route.routeName === 'Camera'){

        return{
            transitionSpec: {
                duration: 300,
                easing: Easing.out(Easing.poly(4)),
                timing: Animated.timing,
                useNativeDriver: true
            },
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const width = layout.initWidth;
                const { index, route } = scene;

                const translateX = position.interpolate({
                    inputRange: [index-1,index, index + 1],
                    outputRange: [-width,0, width],
                });

                return {transform: [{ translateX }] };
            }
        };

    }else{

        return{
            transitionSpec: {
                duration: 300,
                easing: Easing.out(Easing.poly(4)),
                timing: Animated.timing,
                useNativeDriver: true,
            },
            screenInterpolator: sceneProps => {
                const { layout, position, scene } = sceneProps;
                const width = layout.initWidth;
                const { index, route } = scene;

                const translateX = position.interpolate({
                    inputRange: [index-1,index, index + 1],
                    outputRange: [width,0, -width],
                });

                return {transform: [{ translateX }] };
            }
        };

    }

};

const MainStackNavigator = createStackNavigator({
    Feed: {
        screen: HomeScreen,
    },
},{headerMode: 'screen'});

const TabNavigator = createBottomTabNavigator({
    Home: {
      screen: MainStackNavigator,
    },
    Search: Search
},{
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            let iconName;
            if (routeName === 'Home') {
                iconName = `ios-home${focused ? '' : '-outline'}`;
            } else if (routeName === 'Search') {
                iconName = `ios-search${focused ? '' : '-outline'}`;
            }

            // You can return any component that you like here! We usually use an
            // icon component from react-native-vector-icons
            return <Ionicons name={iconName} size={horizontal ? 30 : 35} color={tintColor} />;
        },
    }),
    tabBarOptions: {
        activeTintColor: '#000',
        inactiveTintColor: 'gray',
        showLabel: false,
        style: {paddingTop:10}
    },
});

const HomeStackNavigator = createStackNavigator({
    Camera: {
        screen: CameraScreen,
    },
    Tabs: {
        screen: TabNavigator,
        navigationOptions:{
            header: null
        }
    },
    Direct: Direct
},{
    initialRouteName: 'Tabs',
    headerMode: 'screen',
    cardStyle: { shadowColor: 'transparent' },
    transitionConfig: DinamicTransition,

});

export default class App extends React.Component {
    render() {
        return <HomeStackNavigator />;
    }
}

